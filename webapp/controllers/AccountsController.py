# -*- coding: utf-8 -*-
from tg import expose, flash, require, url, lurl
from tg import request, redirect,session, tmpl_context
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.exceptions import HTTPFound
from webapp.lib.base import BaseController
from webapp.controllers.error import ErrorController
from webapp.lib.app_globals import Globals
from beaker.session import Session

__all__ = ['AccountsController']

class AccountsController(BaseController):
    
    @expose('CreateNewAccount.jinja')
    def CreateNewAccount(self):
        return dict(page='CreateNewAccount')

    @expose('FindAndEditAccount.jinja')
    def FindAndEditAccount(self):
        return dict(page='FindAndEditAccount')
    

    
